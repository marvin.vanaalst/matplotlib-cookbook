# matplotlib cookbook

This repository contains a collection of recipes for working with the [matplotlib](https://matplotlib.org/) python package, that I collected over time. I've tried to keep the interaction with matplotlib as consistent as possible, so that newer users are not confused by the different APIs, however the notebooks are not meant as a beginners guide.

Some of the examples are copied from the matplotlib [documentation](https://matplotlib.org/contents.html), [tutorials](https://matplotlib.org/tutorials/index.html) and [gallery](https://matplotlib.org/gallery/index.html), but the way I work I always found it easier to have them arranged in the way I do in this repository. 

Some examples were also taken from Jake VanderPlas's brilliant [Python Data Science Handbook](https://jakevdp.github.io/PythonDataScienceHandbook/index.html). Go check out his work and if possible support him by [buying the book](http://shop.oreilly.com/product/0636920034919.do).

## Contributing

Feel very free to add anything useful you find. I only ask you to stay away from the pyplot interface (`plt.plot` etc), as it is [generally less-flexible than the object-oriented API](https://matplotlib.org/tutorials/introductory/pyplot.html#sphx-glr-tutorials-introductory-pyplot-py).